import React, {useState, useEffect} from 'react';
import "./accordion1.css";
const AccordionFirst8 = () => {
    const [search, handleSearch] = useState('');
    const [users, handleUsers] = useState([]);
    const [active, setActive] = useState(false);
    const [completed, setCompleted] = useState([]);
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(json => handleUsers(json));
    }, []);
    let usersName = users.filter(
        user => user.name.indexOf(search) < 10
    ).map((user, index) => <div key={index}>{user.name}</div>);
    let usersEmail = users.filter(
        user => user.email.indexOf(search) < 10
    ).map((user, index) => <div key={index}>{user.email}</div>);
    let usersUsername = users.filter(
        user => user.username.indexOf(search) < 10
    ).map((user, index) => <div key={index}>{user.username}</div>);
    let usersPhone = users.filter(
        user => user.phone.indexOf(search) < 10
    ).map((user, index) => <div key={index}>{user.phone}</div>);
    let usersWebsite = users.filter(
        user => user.website.indexOf(search) < 10
    ).map((user, index) => <div key={index}>{user.website}</div>);

    return (
        <div className="accordion1">
            <div className="accordion1__modalwindow">
                <div className="accordion1__modalwindow-open">
                    <div className="accordion1__modalwindow_title">{usersName[7]}</div>
                    <button onClick={() => setActive(true)} className="accordion1__modalwindow_button-open">Open Window
                    </button>
                </div>

                {active && (
                    <div className="accordion1__modalwindow_door">
                        <h5 className="accordion1__modalwindow_door-text">Email: {usersEmail[7]}</h5>
                        <h5 className="accordion1__modalwindow_door-text">Username: {usersUsername[7]}</h5>
                        <h5 className="accordion1__modalwindow_door-text">Phone: {usersPhone[7]}</h5>
                        <h5 className="accordion1__modalwindow_door-text">Website: {usersWebsite[7]}</h5>


                        <button onClick={() => setActive(false)}
                                className="accordion1__modalwindow_door-button">Close Window X
                        </button>
                    </div>

                )}
            </div>


        </div>

    );
};
export default AccordionFirst8;